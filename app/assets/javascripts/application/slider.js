new Glide('.glide', {
    type: 'carousel',
    startAt: 0,
    perView: 3,
    peek: 100,
    breakpoints: {
        960: {
            perView: 2,
            peek: 50
        },
        599: {
            perView: 1,
            peek: 30
        }
    }
}).mount()