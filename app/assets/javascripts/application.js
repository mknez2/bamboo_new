//= require shared/shared
//= require_tree ./application

const openMenuIcon = document.querySelector('.navigation__icon--menu')
const closeMenuIcon = document.querySelector('.navigation__icon--close')
const menu = document.querySelector('.navigation')

function setStylesOnElement(element, styles) {
    Object.assign(element.style, styles)
}

openMenuIcon.addEventListener('click', openMenu);

closeMenuIcon.addEventListener('click', closeMenu);

function openMenu() {
    setStylesOnElement(menu, {'left': '0'})
}

function closeMenu() {
    setStylesOnElement(menu, {'left': '-110%'})