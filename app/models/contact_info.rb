class ContactInfo < ApplicationRecord
	include LogActions

	validates_presence_of :name, :address, :email
	validates :longitude, inclusion: -180..180, allow_nil: true
	validates :latitude, inclusion: -90..90, allow_nil: true

end
