class PagesController < ApplicationController

  def home
  end

  def contact
    @contact = ContactInfo.first
  end

end
