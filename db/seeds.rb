# look up "lib/modules/seed_methods.rb"
include SeedMethods
# puts create_title(10..70)
# puts create_paragraphs((1..4), true)

Admin.create(first_name: "Bamboo",
  last_name: "Lab",
  password: "vise nije adminadmin",
  superadmin: true,
  email: "contact@bamboolab.eu")

ContactInfo.create(name: "BambooLab",
	address: "Ul. Josipa Jurja Strossmayera 341, 31000, Osijek",
	email: "contact@bamboolab.eu")
