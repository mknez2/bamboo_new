Rails.application.routes.draw do

  devise_for :admins
  root "pages#home"
  get "contact", to: "pages#contact"
  post "/tinymce_assets", to: "tinymce_assets#create"
  get "cookies", to: "pages#cookies", as: "cookies"
  get "toolkit", to: "toolkit#index"

  resources :contact_messages, only: :create do
    collection { post :validate }
  end

  %w( 404 422 500 ).each do |code|
    get code, :to => "errors#show", :code => code
  end

  namespace :admin do
    root "action_logs#index"
    get "toolkit", to: "toolkit#index"
    resources :admins
    resource :admin_account,                only: [:show, :update], path: :account
    resources :contact_infos,               only: [:index, :create, :edit, :update]
    resources :contact_messages,            only: [:index, :show]
    resources :action_logs,                 only: [:index, :show]
  end

end
